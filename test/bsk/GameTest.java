package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	Game game = new Game();
	Frame frame0, frame1, frame2, frame3, frame4, frame5, frame6, frame7, frame8, frame9;
	
	@Test
	public void testGameGetFrameFromIndex() throws BowlingException  {
		frame0 = new Frame(1,5);
		game.addFrame(frame0);
		
		frame1 = new Frame(3,6);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		
		Frame frame = game.getFrameAt(0);
		assertTrue(frame.equals(frame0));
	}
	
	@Test
	public void testGameCalculateScore() throws BowlingException  {
		frame0 = new Frame(1,5);
		game.addFrame(frame0);
		
		frame1 = new Frame(3,6);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		
		int score = game.calculateScore();
		assertEquals(81,score);
	}
	
	@Test
	public void testGameCalculateScoreWhitSingleSpare() throws BowlingException  {
		frame0 = new Frame(1,9);
		game.addFrame(frame0);
		
		frame1 = new Frame(3,6);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		assertEquals(88,game.calculateScore());
	}
	
	@Test
	public void testGameCalculateScoreWhitSingleStrike() throws BowlingException  {
		frame0 = new Frame(10,0);
		game.addFrame(frame0);
		
		frame1 = new Frame(3,6);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		
		assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testGameCalculateScoreWhitStrikeAndSpare() throws BowlingException  {
		frame0 = new Frame(10,0);
		game.addFrame(frame0);
		
		frame1 = new Frame(4,6);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testGameCalculateScoreWithTwoConsecutiveStrikes() throws BowlingException  {
		frame0 = new Frame(10,0);
		game.addFrame(frame0);
		
		frame1 = new Frame(10,0);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testGameCalculateScoreWithThreeConsecutiveStrikes() throws BowlingException  {
		frame0 = new Frame(10,0);
		game.addFrame(frame0);
		
		frame1 = new Frame(10,0);
		game.addFrame(frame1);
		
		frame2 = new Frame(10,0);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		
		assertEquals(129,game.calculateScore());
	}
	
	@Test
	public void testGameCalculateScoreWithTwoConsecutiveSpare() throws BowlingException  {
		frame0 = new Frame(8,2);
		game.addFrame(frame0);
		
		frame1 = new Frame(5,5);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,6);
		game.addFrame(frame9);
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testGameGetFirstBonusThrow() throws BowlingException  {
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testGameCalculateScoreWithFinalSpare() throws BowlingException  {
		frame0 = new Frame(1,5);
		game.addFrame(frame0);
		
		frame1 = new Frame(3,6);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(2,8);
		game.addFrame(frame9);
		
		game.setFirstBonusThrow(7);
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testGameGetSecondBonusThrow() throws BowlingException  {
		game.setSecondBonusThrow(7);
		assertEquals(7, game.getSecondBonusThrow());
	}
	
	@Test
	public void testGameCalculateScoreWithFinalStrike() throws BowlingException  {
		frame0 = new Frame(1,5);
		game.addFrame(frame0);
		
		frame1 = new Frame(3,6);
		game.addFrame(frame1);
		
		frame2 = new Frame(7,2);
		game.addFrame(frame2);
		
		frame3 = new Frame(3,6);
		game.addFrame(frame3);
		
		frame4 = new Frame(4,4);
		game.addFrame(frame4);
		
		frame5 = new Frame(5,3);
		game.addFrame(frame5);
		
		frame6 = new Frame(3,3);
		game.addFrame(frame6);
		
		frame7 = new Frame(4,5);
		game.addFrame(frame7);
		
		frame8 = new Frame(8,1);
		game.addFrame(frame8);
		
		frame9 = new Frame(10,0);
		game.addFrame(frame9);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testGameCalculateScorePerfectGame() throws BowlingException  {
		frame0 = new Frame(10,0);
		game.addFrame(frame0);
		
		frame1 = new Frame(10,0);
		game.addFrame(frame1);
		
		frame2 = new Frame(10,0);
		game.addFrame(frame2);
		
		frame3 = new Frame(10,0);
		game.addFrame(frame3);
		
		frame4 = new Frame(10,0);
		game.addFrame(frame4);
		
		frame5 = new Frame(10,0);
		game.addFrame(frame5);
		
		frame6 = new Frame(10,0);
		game.addFrame(frame6);
		
		frame7 = new Frame(10,0);
		game.addFrame(frame7);
		
		frame8 = new Frame(10,0);
		game.addFrame(frame8);
		
		frame9 = new Frame(10,0);
		game.addFrame(frame9);
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300, game.calculateScore());
	}
	
	@Test (expected=BowlingException.class)
	public void testGameCalculateError() throws BowlingException  {
		frame0 = new Frame(0,0);
		game.addFrame(frame0);
		
		frame1 = new Frame(0,0);
		game.addFrame(frame1);
		
		frame2 = new Frame(0,0);
		game.addFrame(frame2);
		
		frame3 = new Frame(0,0);
		game.addFrame(frame3);
		
		frame4 = new Frame(0,0);
		game.addFrame(frame4);
		
		frame5 = new Frame(0,0);
		game.addFrame(frame5);
		
		frame6 = new Frame(0,0);
		game.addFrame(frame6);
		
		frame7 = new Frame(0,0);
		game.addFrame(frame7);
		
		frame8 = new Frame(0,0);
		game.addFrame(frame8);
		
		frame9 = new Frame(0,0);
		game.addFrame(frame9);
		
		game.calculateScore();
	}

}
