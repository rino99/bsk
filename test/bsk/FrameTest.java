package bsk;

import static org.junit.Assert.*;
import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;

public class FrameTest {

	@Test
	public void testSingleFrameGetFirstThrow() throws BowlingException{
		Frame frame = new Frame(5,2);
		assertEquals(5,frame.getFirstThrow());
	}
	
	@Test
	public void testSingleFrameGetSecondThrow() throws BowlingException{
		Frame frame = new Frame(5,2);
		assertEquals(2,frame.getSecondThrow());
	}
	
	@Test
	public void testSingleFrameGetScore() throws BowlingException{
		Frame frame = new Frame(5,2);
		assertEquals(7,frame.getScore());
	}
	
	@Test
	public void testFrameGetBonus() throws BowlingException{
		Frame frame = new Frame(9,1);
		frame.setBonus(9);
		assertEquals(9,frame.getBonus());
	}
	
	@Test
	public void testFrameIsSpare() throws BowlingException{
		Frame frame = new Frame(9,1);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testFrameGetScore() throws BowlingException{
		Frame frame = new Frame(9,1);
		frame.setBonus(5);
		assertEquals(15,frame.getScore());
	}
	
	@Test
	public void testFrameIsStrike() throws BowlingException{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testFrameIsNotStrike() throws BowlingException{
		Frame frame = new Frame(8,2);
		assertFalse(frame.isStrike());
	}
	
	@Test
	public void testFrameGetScoreStrike() throws BowlingException{
		Frame frame0 = new Frame(10,0);
		Frame frame1 = new Frame(3,6);
		frame0.setBonus(frame1.getFirstThrow() + frame1.getSecondThrow());
		assertEquals(19, frame0.getScore());
	}
	
	@Test(expected=BowlingException.class)
	public void testSingleFrameError() throws BowlingException{
		new Frame(20,45);
	}

}
