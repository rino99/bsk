package tdd.training.bsk;

public class Frame {
	private int firstThrow;
	private int secondThrow;
	private int bonus = 0;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(checkData(firstThrow,secondThrow)) {
			this.firstThrow = firstThrow;
			this.secondThrow = secondThrow;
		}else {
			throw new BowlingException();
		}
		
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return this.bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		int scoreTemp = this.firstThrow + this.secondThrow;
		if(this.isSpare() || this.isStrike()) {
			return scoreTemp + this.bonus;
		}else {
			return scoreTemp;
		}
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return (this.getFirstThrow() == 10 && this.getSecondThrow() == 0);
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return ( (this.getFirstThrow() + this.getSecondThrow()) == 10);
	}
	
	/**
	 * It returns true if value are in the correct range.
	 * 
	 * @return <true> if in-range, <false> over-range.
	 */
	private boolean checkData(int firstThrow, int secondThrow) {
		return (firstThrow >= 0 && firstThrow <= 10 && secondThrow >= 0 && secondThrow <= 10);
	}

}
