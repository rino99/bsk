package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	ArrayList<Frame> gameList;
	private int firstBonusThrow;
	private int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		 this.gameList = new ArrayList<>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frame != null) {
			this.gameList.add(frame);
		}else {
			throw new BowlingException();
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index >= 0 && index <= 9) {
			return gameList.get(index);	
		}else {
			throw new BowlingException();
		}
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(firstBonusThrow != 0) {
			this.firstBonusThrow = firstBonusThrow;
		}else {
			throw new BowlingException();
		}	
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(secondBonusThrow != 0) {
			this.secondBonusThrow = secondBonusThrow;
		}else {
			throw new BowlingException();
		}	
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int i = 0; i < gameList.size(); i++) {
			if(gameList.get(i).isSpare() && isInRange(i)) {
				gameList.get(i).setBonus(gameList.get(i+1).getFirstThrow());
			}
			if(gameList.get(i).isStrike() && isInRange(i)) {
				couldBeDoubleStrike(i);
			}
			score += gameList.get(i).getScore();
		}
		
		score += lastFrameCouldBeSpareOrStrike();
		
		if (score == 0) {
			throw new BowlingException();
		}else {
			return score;
		}
	}

	private boolean isInRange(int i) {
		return (i != 9);
	}
	
	private void couldBeDoubleStrike(int i) {
		if(gameList.get(i+1).isStrike()) {
			setBonusDoubleStrike(i);
		}else {
			setBonusSingleStrike(i);
		}
	}
	
	private void setBonusDoubleStrike(int i) {
		switch(i) {
			case 8:
				setBonusStrikeEighthFrame(i);
			break;
			case 9:
				setBonusStrikeNinthFrame(i);
			break;
			default:
				gameList.get(i).setBonus(gameList.get(i+1).getFirstThrow() + gameList.get(i+2).getFirstThrow());	
			break;
		}
	}
	
	private void setBonusSingleStrike(int i) {
		gameList.get(i).setBonus(gameList.get(i+1).getFirstThrow() + gameList.get(i+1).getSecondThrow());
	}
	
	private int lastFrameCouldBeSpareOrStrike() {
		if(gameList.get(9).isSpare()) {
			if(gameList.get(9).isStrike()) {
				return getFirstBonusThrow() + getSecondBonusThrow();
			}else {
				return getFirstBonusThrow();
			}
		}
		return 0;
	}
	
	private void setBonusStrikeEighthFrame(int i) {
		gameList.get(i).setBonus(gameList.get(i+1).getFirstThrow() + this.getFirstBonusThrow());	
	}
	
	private void setBonusStrikeNinthFrame(int i) {
		gameList.get(i).setBonus(this.getFirstBonusThrow() + this.getSecondBonusThrow());	
	}
	
}
